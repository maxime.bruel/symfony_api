<?php

namespace App\Entity;

use App\Repository\RessourceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=RessourceRepository::class)
 */
class Ressource
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("ressource:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("ressource:read")
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     * @Groups("ressource:read")
     */
    private $image;

    /**
     * @ORM\Column(type="text")
     * @Groups("ressource:read")
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @Groups("ressource:read")
     */
    private $quantity_total;

    /**
     * @ORM\ManyToMany(targetEntity=Group::class, mappedBy="ressources")
     */
    private $groups;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, mappedBy="ressources")
     */
    private $categories;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getQuantityTotal(): ?int
    {
        return $this->quantity_total;
    }

    public function setQuantityTotal(int $quantity_total): self
    {
        $this->quantity_total = $quantity_total;

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroup(Group $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
            $group->addRessource($this);
        }

        return $this;
    }

    public function removeGroup(Group $group): self
    {
        if ($this->groups->removeElement($group)) {
            $group->removeRessource($this);
        }

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addRessource($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->removeElement($category)) {
            $category->removeRessource($this);
        }

        return $this;
    }
}
