<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Group;
use App\Entity\Category;
use App\Entity\Ressource;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // Categories
        $category = new Category();
        $category->setLabel('Informatique');
        $manager->persist($category);
        $group = new Group();
        $group->setLabel('MMI');
        $manager->persist($group);

        // Groups
        $group = new Group();
        $group->setLabel('LPMDN');
        $manager->persist($group);

        for ($i = 0; $i < 20; $i++) {

            // Users
            $user = new User();
            $user->setFirstname('prenom '.$i);
            $user->setLastname('nom '.$i);
            $user->setEmail('prenom'.$i.'@gmail.com');
            $user->setPassword(password_hash('mdp'.$i, PASSWORD_DEFAULT));
            $user->setGroupId($group);
            $manager->persist($user);

            // Ressources
            $ressource = new Ressource();
            $ressource->setLabel('ressource '.$i);
            $ressource->setDescription('description '.$i);
            $ressource->setQuantitytotal(5);
        }

        $manager->flush();
    }
}
