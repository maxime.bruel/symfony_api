<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApiController extends AbstractController
{
    #[Route('/api', name: 'api_documentation')]
    public function dashboard(): Response
    {
        return $this->render('api/documentation.html.twig', [
            'controller_name' => 'ApiController',
        ]);
    }

    #[Route('/api/user', name: 'api_user_get', methods: ["GET"])]
    public function indexUserGet(UserRepository $userRepository): Response
    {
        return $this->json($userRepository->findAll(), 200, [], ['groups' => 'user:read']);
    }

    #[Route('/api/user/{id}', name: 'api_user_get_id', methods: ["GET"])]
    public function indexUserGetId(User $user, UserRepository $userRepository): Response
    {
        return $this->json($userRepository->find($user), 200, [], ['groups' => 'user:read']);
    }

    #[Route('/api/user', name: 'api_user_post', methods: ["POST"])]
    public function indexUserPost(Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager, ValidatorInterface $validator): Response
    {
        $json = $request->getContent();
        try {
            $user = $serializer->deserialize($json, User::class, 'json');
            $user->setPassword(password_hash($user->getPassword(), PASSWORD_DEFAULT));
            $errors = $validator->validate($user);
            if (count($errors) > 0) {
                return $this->json($errors, 400);
            }
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->json($user, 201, [], ['groups' => 'user:read']);
        } catch (NotEncodableValueException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }

    #[Route('/api/user/{id}', name: 'api_user_update', methods: ["PATCH"])]
    public function indexUserUpdate(User $user, Request $request, EntityManagerInterface $entityManager, ValidatorInterface $validator): Response
    {
        $json = json_decode($request->getContent());
        try {
            if (!empty($json->firstname)) {
                $user->setFirstname($json->firstname);
            }
            if (!empty($json->lastname)) {
                $user->setLastname($json->lastname);
            }
            if (!empty($json->email)) {
                $user->setEmail($json->email);
            }
            if (!empty($json->password)) {
                $user->setPassword(password_hash($json->password, PASSWORD_DEFAULT));
            }
            $errors = $validator->validate($user);
            if (count($errors) > 0) {
                return $this->json($errors, 400);
            }
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->json($user, 200, [], ['groups' => 'user:read']);
        } catch (NotEncodableValueException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }

    #[Route('/api/user/{id}', name: 'api_user_delete', methods: ["DELETE"])]
    public function indexUserDelete(User $user, EntityManagerInterface $entityManager): Response
    {
        $entityManager->remove($user);
        $entityManager->flush();
        return $this->json($user, 204, [], ['groups' => 'user:read']);
    }
}
