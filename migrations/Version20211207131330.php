<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211207131330 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE loan ADD user_id_id INT NOT NULL, ADD ressource_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE loan ADD CONSTRAINT FK_C5D30D039D86650F FOREIGN KEY (user_id_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE loan ADD CONSTRAINT FK_C5D30D03EBD01AD3 FOREIGN KEY (ressource_id_id) REFERENCES ressource (id)');
        $this->addSql('CREATE INDEX IDX_C5D30D039D86650F ON loan (user_id_id)');
        $this->addSql('CREATE INDEX IDX_C5D30D03EBD01AD3 ON loan (ressource_id_id)');
        $this->addSql('ALTER TABLE user ADD group_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6492F68B530 FOREIGN KEY (group_id_id) REFERENCES `group` (id)');
        $this->addSql('CREATE INDEX IDX_8D93D6492F68B530 ON user (group_id_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE loan DROP FOREIGN KEY FK_C5D30D039D86650F');
        $this->addSql('ALTER TABLE loan DROP FOREIGN KEY FK_C5D30D03EBD01AD3');
        $this->addSql('DROP INDEX IDX_C5D30D039D86650F ON loan');
        $this->addSql('DROP INDEX IDX_C5D30D03EBD01AD3 ON loan');
        $this->addSql('ALTER TABLE loan DROP user_id_id, DROP ressource_id_id');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D6492F68B530');
        $this->addSql('DROP INDEX IDX_8D93D6492F68B530 ON `user`');
        $this->addSql('ALTER TABLE `user` DROP group_id_id');
    }
}
